$(() => {
    console.log("ready")
    let lineItemCount = 1

    addInputLine()

    $("#new-line").on("click", () => {
        addInputLine()
    })

    $("textarea").on("change", function() {
        checkAreaFit(this)
    })
})

addInputLine = () => {
    let reviewInputs = $("#review-inputs")
    let lastIndex = 1
    let newIndex = lastIndex

    if (reviewInputs.children().length > 0) {
        lastIndex = reviewInputs.children().last().attr("index")
        newIndex = parseInt(lastIndex) + 1
    }

    let reviewInput = $(document.createElement("div")).attr({
        "class": "review-input",
        index: newIndex
    })

    let reviewLabel = $("<label/>").text(`Line ${newIndex}`)
    let reviewTextInput = $("<input/>").attr({
        type: "textbox",
        maxlength: 40
    }).keypress(function() {
        handleInput(this)
    }).keyup(function() {
        handleTextAreaAndLineNumbers()
    })
    let reviewDeleteButton = $("<button/>").text("X").on("click", () => {
        removeInputLine(reviewInput)
    })

    $(reviewInput).append(reviewLabel)
    $(reviewInput).append(reviewTextInput)
    $(reviewInput).append(reviewDeleteButton)

    reviewInputs.append(reviewInput)

    checkMaxInput()

    reviewTextInput.focus()
}

handleInput = (input) => {
    const totalLines = $("#review-inputs").children().length
    if (input.value.length == 40 && totalLines < 25) {
        addInputLine()
    }

    checkMaxInput()
}

checkMaxInput = () => {
    const totalLines = $("#review-inputs").children().length

    if (totalLines === 25) {
        $("#new-line").prop("disabled", true)
    } else {
        $("#new-line").prop("disabled", false)
    }
}

handleTextAreaAndLineNumbers = (updateLines) => {
    let output = ""
    var lastIndex = 0

    $(".review-input").each(function() {
        const input = $(this).find("input")
        const content = input.val()
        if (content.length > 0) {
            output += content
        }
        if (updateLines) {
            const labelItem = $(this).find("label")
            const index = $(this).attr("index")
            let nextIndex = parseInt(lastIndex) + 1
            if (nextIndex != parseInt(index)) {
                labelItem.text(`Line ${nextIndex}`)
                $(this).attr("index", nextIndex)
            }
            lastIndex = nextIndex
        }
    })

    $("textarea").val(output)
    $("textarea").change()
}

removeInputLine = (inputToDelete) => {
    if (inputToDelete.find("input").val().length > 0) {
        const confirmed = window.confirm("This line has text in it. Are you sure you want to delete it?")

        if (!confirmed) {
            return
        }
    }

    inputToDelete.remove()

    handleTextAreaAndLineNumbers(true)

    checkMaxInput()
}

checkAreaFit = (area) => {
    const areaItem = $(area)
    let fontSize = areaItem.css("font-size")
    var textHeight = $("#text-height").text($("textarea").val());
    textHeight.css("font-size", `${parseInt(fontSize.substr(0,2)) + 1}px`)

    if (textHeight[0].clientHeight > areaItem[0].scrollHeight) {
        $(area).css("font-size", `${parseInt(fontSize.substr(0,2)) - 1}px`)
    }
}